# object-oriented-cpp

This project holds the assignments done for the course "cs-221 Object Oriented Programming"

## Project Structure

- Every folder name (or assignment) is a date format (dd/mm/yyyy) on which the work was assigned. Example: `16-03-2021`
- Every assignment has a `Makefile` to easily test the programs.
- The `Makefile`'s target names are in format `question_<int>`. Example: `question_1`

## How to test

- clone this repo
  ```sh
  git clone https://gitlab.com/siddhantCodes/object-oriented-cpp.git
  ```

- Inside the git repo directory, cd to any assignment directory.
  > The assignment folder name is the date on which the work was assigned.

  ```sh
  cd 16-03-2021
  ```

- Run `make question_1` to build the question_1.cpp file.
- Run `./question_1.program` to run the program.

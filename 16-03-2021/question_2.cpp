#include <cstdint>
#include <cstdlib>
#include <iostream>

// 2. Write a program to test whether the number entered by the user is 
// positive or negative.

int main () {
  // take user input
  int num;

  std::cout << "Enter the number: ";
  std::cin >> num;
  std::cout << std::endl;

  // check for invalid inputs
  if (!num) {
    std::cout << "invalid input, only integer values are accepted";
    exit(1);
  }

  if (num >= 0) {
    // num is positive
    std::cout << "Number " << num << " is positive";
  } else {
    // num is negative
    std::cout << "Number " << num << " is negative";
  }
}

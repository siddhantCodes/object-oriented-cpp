#include <iostream>

// 3. Write a program to compare 3 numbers a, b, and c entered by the user to
// find the greatest among them.
// You can implement it using nested-ifs or if-else-if ladder.

int main() {
  // take input
  int n1, n2, n3;

  std::cout << "Enter first number: ";
  std::cin >> n1;
  std::cout << std::endl;

  std::cout << "Enter second number: ";
  std::cin >> n2;
  std::cout << std::endl;

  std::cout << "Enter thirs number: ";
  std::cin >> n3;
  std::cout << std::endl;

  if (n1 > n2) {
    // n1 is greater
    if (n1 > n3)
      // n1 is greater
      std::cout << "First number " << n1 << " is greatest";
    else
      // n3 is greater
      std::cout << "Third number " << n3 << " is greatest";
  } else {
    // n2 is greater
    if (n2 > n3)
      std::cout << "Second number " << n2 << " is greatest";
    else
      // n3 is greater
      std::cout << "Third number " << n3 << " is greatest";
  }
}

#include <iostream>

/*
4. Write a program to assign grades to students' marks according
to the following scheme:

Marks Grade
> 90 and <= 100 A
> 80 and <= 90 B
> 60 and <= 80 C
> 40 and <=60 D
<=40 E

The marks will be entered by the user.
*/

int main() {

  for (;;) {
    int marks;

    std::cout << "type any non integer or 0 to exit." << std::endl;
    std::cout << "Enter marks: ";
    std::cin >> marks;

    if (!marks) {
      break;
    }

    if (marks <= 40)
      std::cout << "E" << std::endl;

    if (marks > 40 && marks <= 60)
      std::cout << "D" << std::endl;

    if (marks > 60 && marks <= 80)
      std::cout << "C" << std::endl;

    if (marks > 80 && marks <= 90)
      std::cout << "B" << std::endl;
    if (marks > 90 && marks <= 100)
      std::cout << "A" << std::endl;
  }
}

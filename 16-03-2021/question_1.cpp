#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <math.h>

// 1. Write a program to print the area and volume of a sphere. 
// The formulae are: area = 4 * pi * r * r * r, volume = 4/3 * pi * r * r * r.

int main () {
  // take user input
  int radius;

  std::cout << "Enter the radius of sphere: ";
  std::cin >> radius;
  std::cout << std::endl;

  // check for invalid inputs
  if (!radius) {
    std::cout << "invalid input, only integer values are accepted";
    exit(1);
  }

  float area = 4.0 * M_PI * radius * radius * radius;
  float volume = area / 3.0;

  printf(
      "Total Surface Area of sphere of radius %d is %f\n", 
      radius, 
      area
  );

  printf(
      "Volume of sphere of radius %d is %f\n", 
      radius, 
      volume
  );
}

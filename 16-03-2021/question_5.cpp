#include <iostream>

/*
5. Write a program to print the following pattern on the screen:

1
2 3
4 5 6
7 8 9 10
*/

int main() {
  int num = 1;

  for (int i = 1; i <= 5; i++) {
    for (int j = 1; j <= i; j++) {
      std::cout << num << " ";
      num++;
    }

    std::cout << std::endl;
  }
}

#include <iostream>

/*
 * Write a program to input two numbers from the user. Pass these two numbers to
 * a function 'add()' using call by reference. This function should then return
 * the sum of the two numbers passed to it.
 */

int add(int *a, int *b);

int main() {
  int a, b;

  std::cout << "Enter an integer and press enter:\t";
  std::cin >> a;
  std::cout << "Enter another integer and press enter:\t";
  std::cin >> b;

  printf("\nsum is %d\n", add(&a, &b));
}

int add(int *a, int *b) { return *a + *b; }

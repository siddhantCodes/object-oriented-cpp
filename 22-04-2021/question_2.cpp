#include <iostream>

// Write a function 'maxArray()' which receives an array of doubles and returns
// the maximum value in the array. You can assume the array contains at least
// one element. The prototype would be double maxArray(double dar[], int size);

double maxArray(double dar[], int size);

int main() {
  double test[] = {2.34, 2.22, 3.99, 9.001, 10.00001, 8, 211.21, 3212.21};

  int size = sizeof(test) / sizeof(test[0]);

  printf("Greatest element is: %f", maxArray(test, size));
}

double maxArray(double dar[], int size) {
  double result = 0;

  for (int i = 0; i <= size; i++) {
    if (dar[i] > result)
      result = dar[i];
  }

  return result;
}

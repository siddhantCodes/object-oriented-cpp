## 5. What do you mean by friend functions? Explain with the help of suitable example(s).

Friend functions, similar to friend classes can access private members of a class
in which they are declared as a friend function.

Example:

```cpp
#include <iostream>

class Lion {
public:
  void Kill() { std::cout << "Kill this animal!" << std::endl; }
};

class Animal {
private:
  int living;
  friend void Lion::Kill();
};
```

In the above example, the method `Kill` in the `Lion` class has access to all the private members of the `Animal` class.
We did this by declaring the `Kill` method of class `Lion` as the friend function inside the `Animal` class.

So, a method that is declared as a friend in a class has access to all the private members of that class.

### Following are some important points about friend functions and classes: 

1. Friends should be used only for limited purpose. too many functions or external classes are declared as friends of a class with protected or private data, 
  it lessens the value of encapsulation of separate classes in object-oriented programming.
2. Friendship is not mutual. If class A is a friend of B, then B doesn’t become a friend of A automatically.
3. Friendship is not inherited.

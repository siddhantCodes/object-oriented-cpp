#include <iostream>

// Write a program to print the area of two rectangles having sides (4,5) and
// (5,8) respectively, by creating a class named 'Rectangle' with a function
// named 'Area()' which returns the area. Length and breadth are passed as
// parameters to its constructor.

class Rectangle {
  int l;
  int b;

public:
  Rectangle(int l, int b) {
    this->l = l;
    this->b = b;
  }

  int Area() { return this->l * this->b; }
};

int main() {
  Rectangle *rect1 = new Rectangle(4, 5);
  Rectangle *rect2 = new Rectangle(5, 8);

  std::cout << "Area of rect(4,5) is: " << rect1->Area() << std::endl;
  std::cout << "Area of rect(5,8) is: " << rect2->Area() << std::endl;

  delete rect1;
  delete rect2;
}

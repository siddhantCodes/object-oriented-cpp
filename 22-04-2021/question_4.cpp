#include <iostream>

// Create a class that imitates part of the functionality of the basic data type
// int. Call the class 'Int' (note different capitalization). The only data in
// this class is an int variable. Include member functions to initialize an Int
// to 0, to initialize it to an int value, to display it (it looks just like an
// int), and to add two Int values. Write a program that exercises this class by
// creating one uninitialized and two initialized Int values, adding the two
// initialized values and placing the response in the uninitialized value, and
// then displaying this result.
//
// At the point of writing this answer, I did not know about the operator 
// overloading feature of cpp that could be used here for the Add(int value)
// method. Still, I am leaving my original implementation as I kinda like the
// chained API approach.

class Int {
  int data;

public:
  Int(int value) { this->data = value; }
  Int() { this->data = 0; }

  void Set(int value) { this->data = value; }

  int Get() { return this->data; }

  Int* Add(int value) {
    this->data += value;
    return this;
  }

  /* Alternative:
  Int operator + (Int const &obj) {
    Int temp;
    temp.data = this->data + obj.data;
    return temp;
  }
  */
};

int main() {

  Int *result = new Int();
  Int *a = new Int(4);
  Int *b = new Int(5);

  result->Set(a->Add(b->Get())->Get());

  // should print 9
  std::cout << "Result: " << result->Get() << std::endl;
}
